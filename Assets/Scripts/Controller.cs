﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	public float dragSpeed;
	//private float moveSpeed = 20;
	private RaycastHit hit;
	private Spinner spinner;
	public float speed;
	private Vector2 startPos;
	private Vector2 endPos;
	private float timeStarted;
	public Spinner[] spinners;


	// Use this for initialization
	void Start () {
		spinner = null;
		startPos = Vector2.zero;
		endPos = Vector2.zero;
		dragSpeed = 0;
	}

	// Update is called once per frame
	void Update () {

		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) {
			Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
			if (Physics.Raycast (ray, out hit)) {
				spinner = hit.transform.GetComponent<Spinner> ();
				startPos = Input.GetTouch (0).position;
				timeStarted = Time.time;
			}

		} else if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) {
			if (spinner != null)
				spinner.TurnSection (Input.GetTouch(0).deltaPosition.x / 6f);
		}
		else if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Ended) {
				endPos = Input.GetTouch (0).position;
				float Distance = startPos.x - endPos.x;
				float timeDiff = Time.time - timeStarted;
				timeStarted = 0;
				Speedometer (Distance, timeDiff);
			}
	}

	void Speedometer(float travel, float timeSwiped){
		dragSpeed =  travel / timeSwiped;
		if(Mathf.Abs(dragSpeed) <= 1000)
		if(spinner != null)
			spinner.Reset ();
		if(Mathf.Abs(dragSpeed) > 1000)
			spinner.SpinSection (dragSpeed);
		//print ("speed = " + dragSpeed);
	}

	public void RandoSpin(){
		foreach (Spinner script in spinners) {
			script.RandomSpin();
		}
	}


}
