﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.EventSystems;

public class ColorScene : MonoBehaviour {

	public Texture2D tex;
	public RawImage colorImage;
	public Color color = Color.red;
	private byte[] bytes;
	public bool canPaint = false;
	public Animator anim;
	private bool PanUp = false;
	// Use this for initialization
	void Start () {
		
		#if UNITY_EDITOR
		bytes = File.ReadAllBytes(Application.dataPath + "/Resources/GeneratedImages/SavedScreen.png");
		#endif

		#if UNITY_IOS || UNITY_ANDROID
		bytes = File.ReadAllBytes(Application.persistentDataPath + "/SavedScreen.png");
		#endif

		tex = new Texture2D (Screen.width, Screen.height);
		tex.LoadImage (bytes);
		colorImage.texture = tex;
	}

	public void ColorChoice(Image graphic){
		color = graphic.color;
	}

	public void PanelAnim(){
		PanUp = !PanUp;
		anim.SetBool ("Up", PanUp);
	}


	void Update(){
		
		#if UNITY_EDITOR
		canPaint = EventSystem.current.IsPointerOverGameObject ();
		if (Input.GetMouseButtonDown (0) && !canPaint) {
			tex.FloodFillArea(Mathf.RoundToInt(Input.mousePosition.x), Mathf.RoundToInt(Input.mousePosition.y), color);
			// When you're done call Apply to commit your changes.
			tex.Apply();
		}
		#endif

		#if UNITY_IOS || UNITY_ANDROID
		canPaint = EventSystem.current.IsPointerOverGameObject (Input.GetTouch (0).fingerId);
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && !canPaint) {
			tex.FloodFillArea(Mathf.RoundToInt(Input.GetTouch(0).position.x), Mathf.RoundToInt(Input.GetTouch(0).position.y), color);
			// When you're done call Apply to commit your changes.
			tex.Apply();
		}
		#endif

	}
}
