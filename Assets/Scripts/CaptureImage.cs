﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class CaptureImage : MonoBehaviour {

	private int width;
	private int height;
	private Camera cam;
	public GameObject shuffleButton;
	public GameObject captureButton;

	public void CapImage(){
		width = Screen.width;
		height = Screen.height;
		cam = GetComponent<Camera> ();
		StartCoroutine ("ScreenCap");
	}

	IEnumerator ScreenCap(){
		shuffleButton.SetActive (false);
		captureButton.SetActive (false);
		RenderTexture rt = new RenderTexture (width, height, 24);
		cam.targetTexture = rt;
		Texture2D capture = new Texture2D(width, height, TextureFormat.RGB24, false);
		yield return new WaitForEndOfFrame ();
		//cam.Render ();
		RenderTexture.active = rt;
		capture.ReadPixels (new Rect (0, 0, width, height), 0, 0);
		cam.targetTexture = null;
		RenderTexture.active = null;
		Destroy (rt);
		byte[] bytes =  capture.EncodeToPNG ();
		#if UNITY_EDITOR
		File.WriteAllBytes (Application.dataPath + "/Resources/GeneratedImages/SavedScreen.png", bytes);
		print (Application.dataPath + "/Resources/GeneratedImages/SavedScreen.png");
		#endif
		#if UNITY_IOS || UNITY_ANDROID
		File.WriteAllBytes( Application.persistentDataPath + "/SavedScreen.png", bytes);
		#endif
		yield return new WaitForEndOfFrame ();
		shuffleButton.SetActive (true);
		captureButton.SetActive (true);
		SceneManager.LoadScene (1);
	}
}
