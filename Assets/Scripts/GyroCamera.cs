﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroCamera : MonoBehaviour {

	private bool gyroBool;
	private Gyroscope gyro;
	private Quaternion rotFix;
	private GameObject camGrandparent;

	public bool touchRotatesHeading;
	public bool touchRotatesPitch;
	private Vector2 screenSize;
	private Vector2 mouseStartPoint;
	private float headingAtTouchStart = 0;
	public float heading = 0;
	public float pitch = 0;
	private float pitchAtTouchStart = 0;

	void Start(){
		Transform currentParent = transform.parent;
		GameObject camParent = new GameObject ("camParent");
		// match the transform to the camera position
		camParent.transform.position = transform.position;
		// make the new transform the parent of the camera transform
		transform.parent = camParent.transform;
		// instantiate a new transform
		camGrandparent = new GameObject ("camGrandParent");
		// match the transform to the camera position
		camGrandparent.transform.position = transform.position;
		// make the new transform the grandparent of the camera transform
		camParent.transform.parent = camGrandparent.transform;
		// make the original parent the great grandparent of the camera transform
		camGrandparent.transform.parent = currentParent;
		//Dont destroy Command
		DontDestroyOnLoad (camGrandparent.transform);

		gyroBool = SystemInfo.supportsGyroscope;

		if (gyroBool) {
			gyro = Input.gyro;
			gyro.enabled = true;

			screenSize.x = Screen.width;
			screenSize.y = Screen.height;
		}

		//camParent rotation and direction
		if (Screen.orientation == ScreenOrientation.LandscapeLeft) {
			camParent.transform.rotation = Quaternion.Euler(new Vector3(90,180,90));
			rotFix = new Quaternion(0,0,1,0);
		} else if (Screen.orientation == ScreenOrientation.LandscapeRight) {
			camParent.transform.rotation = Quaternion.Euler( new Vector3(90,0,90));
			rotFix = new Quaternion(0,0,1,0);
		} else if (Screen.orientation == ScreenOrientation.Portrait) {
			camParent.transform.rotation = Quaternion.Euler(new Vector3(90,90,0));
			rotFix = new Quaternion(0,0,1,0);
		} else if (Screen.orientation == ScreenOrientation.PortraitUpsideDown) {
			camParent.transform.rotation = Quaternion.Euler( new Vector3(90,90,0));
			rotFix = new Quaternion(0,0,0,1);
		}else {
		print("NO GYRO");
	}
}

	void Update(){
		if (gyroBool) {
			Quaternion camRot = gyro.attitude * rotFix;
			transform.localRotation = camRot;


		}

		if (touchRotatesHeading) {
			GetTouchMouseInput();
		}
	}

		void GetTouchMouseInput() {
		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) {
				mouseStartPoint = Input.GetTouch(0).position;
				headingAtTouchStart = heading;
				pitchAtTouchStart = pitch;

		} else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
				Vector2 delta = Vector2.zero;
				Vector2 mousePos = Input.GetTouch(0).position;
			if (touchRotatesHeading) {
				delta.x = (mousePos.x - mouseStartPoint.x) / screenSize.x;
				heading = (headingAtTouchStart - delta.x * 100); 
			}if (touchRotatesPitch) {
				delta.y = (mousePos.y - mouseStartPoint.y) / screenSize.y;
				pitch = (pitchAtTouchStart + delta.y * 100);
			}
			Vector3 tempRot = camGrandparent.transform.localRotation.eulerAngles;
			tempRot.y = heading;
			tempRot.z = pitch;
			camGrandparent.transform.localRotation = Quaternion.Euler(tempRot);
			}
	}

}
