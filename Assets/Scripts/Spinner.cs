﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Spinner : MonoBehaviour {
	private Rigidbody Wheel;
	private float spindeg;
	private bool spun = false;
	private float timeTakenDuringLerp = 1f;
	private bool canLerp = false;
	private bool _isLerping;
	private float _startPosition;
	private float _endPosition;
	private float _timeStartedLerping;
	private Vector3 wheelDeg;
	private float[] negPos;
	private TextureSwap textswapper;
	private bool shouldSwap;
	private float startSpin;




	// Use this for initialization
	void Start () {
		textswapper = GameObject.FindGameObjectWithTag ("textureSwapper").GetComponent<TextureSwap>();
		Wheel = transform.GetComponent<Rigidbody> ();
		negPos = new float[2];
		negPos [0] = 1f;
		negPos [1] = -1f;
		startSpin = transform.eulerAngles.y;
	}


	// Update is called once per frame
	void Update () {
		//LERP FINAL SPIN POSITION
		if (canLerp) {
			canLerp = false;
			_isLerping = true;
			_timeStartedLerping = Time.time;
			_startPosition = Wheel.gameObject.transform.rotation.eulerAngles.y;
			_endPosition = Mathf.Round (_startPosition / 60) * 60;
			Wheel.isKinematic = true;
		}
		if(_isLerping)
		{
			float timeSinceStarted = Time.time - _timeStartedLerping;
			float percentageComplete = timeSinceStarted / timeTakenDuringLerp;
			wheelDeg.y = Mathf.Lerp (_startPosition, _endPosition, percentageComplete);
			Wheel.gameObject.transform.rotation = Quaternion.Euler(wheelDeg);
			//When we've completed the lerp, we set _isLerping to false
			if(percentageComplete >= 1.0f)
			{
				startSpin = Wheel.transform.localEulerAngles.y;
				spun = false;
				_isLerping = false;
				Wheel.isKinematic = false;
				textswapper.UpdateTexture (gameObject.tag.ToString());
			}
		}
		//ROUND RESULTING POSITION
		if (Mathf.Abs(Wheel.angularVelocity.y) <= .2f && Mathf.Abs(Wheel.angularVelocity.y) > 0 && spun) {
			canLerp = true;
		}
		}

	public void SpinSection(float spinspeed){
		Wheel.isKinematic = false;
		spindeg = spinspeed;
		Wheel.AddTorque (transform.up * spindeg * 100f);
		spun = true;
		textswapper.Spun (transform.tag.ToString ());
	}

	public void TurnSection(float spinAmount){
		Wheel.isKinematic = true;
		startSpin -= spinAmount;
		Wheel.transform.eulerAngles = new Vector3(0, startSpin, 0);
		//print (moveAmount);
	}

	public void Reset(){
		canLerp = true;
	}

	public void RandomSpin(){
		Wheel.isKinematic = false;
		float force = Random.Range (8500f, 15000f);
		int numbIndex = Random.Range (0, negPos.Length);
		Wheel.AddTorque (transform.up * force * negPos[numbIndex]);
		textswapper.Spun (transform.tag.ToString ());
		spun = true;

	}

}