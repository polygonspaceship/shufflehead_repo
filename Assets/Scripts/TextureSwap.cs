﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TextureSwap : MonoBehaviour {
	//Resource Arrays
	public Text LineOne;
	public Text LineTwo;
	public Text LineThree;
	private Texture[] topTex;
	private Texture[] midTex;
	private Texture[] bottomTex;

	//Lists for all Textures currently available or in use
	public List<Texture> topAvailableTextures = new List<Texture>();
	public List<Texture> topInUseTextures = new List<Texture>();
	public List<Texture> midAvailableTextures = new List<Texture>();
	public List<Texture> midInUseTextures = new List<Texture>();
	public List<Texture> bottomAvailableTextures = new List<Texture>();
	public List<Texture> bottomInUseTextures = new List<Texture>();
	public List<Texture> topTempTextures = new List<Texture>();
	public List<Texture> midTempTextures = new List<Texture>();
	public List<Texture> bottomTempTextures = new List<Texture>();

	public string topTextureName;
	public string midTextureName;
	public string bottomTextureName;

	//Spinner and Material Arrays
	private GameObject topSpinner;
	private GameObject midSpinner;
	private GameObject bottomSpinner;
	public Material[] topMats;
	public Material[] midMats;
	public Material[] bottomMats;

	private AudioManager audioManager;



	void Awake(){
		topSpinner = GameObject.FindGameObjectWithTag ("topSpinner");
		midSpinner = GameObject.FindGameObjectWithTag ("midSpinner");
		bottomSpinner = GameObject.FindGameObjectWithTag ("bottomSpinner");
		//Textures Load
		topTex = Resources.LoadAll<Texture>("TopExports");
		midTex = Resources.LoadAll<Texture>("MidExports");
		bottomTex = Resources.LoadAll<Texture>("BottomExports");
		//Materials Load
		topMats = topSpinner.GetComponent<MeshRenderer> ().materials;
		midMats = midSpinner.GetComponent<MeshRenderer> ().materials;
		bottomMats = bottomSpinner.GetComponent<MeshRenderer> ().materials;
		audioManager = GameObject.Find ("AudioManager").GetComponent<AudioManager> ();
	}
		
	// Use this for initialization
	void Start () {
		//Top Section
		foreach (Texture file in topTex) {
			topAvailableTextures.Add (file);
		}
		foreach (Material mat in topMats) {
			if (mat.name != "Middles (Instance)") {
				mat.mainTexture = topAvailableTextures [Random.Range (0, topAvailableTextures.Count - 1)];
				topAvailableTextures.Remove (mat.mainTexture);
				topInUseTextures.Add (mat.mainTexture);
			}
		} 
		//Middle Section
		foreach (Texture file in midTex) {
			midAvailableTextures.Add (file);
		}
		foreach (Material mat in midMats) {
			if (mat.name != "Middles (Instance)") {
				mat.mainTexture = midAvailableTextures [Random.Range (0, midAvailableTextures.Count - 1)];
				midAvailableTextures.Remove (mat.mainTexture);
				midInUseTextures.Add (mat.mainTexture);
			}
		} 
		//Bottom Section
		foreach (Texture file in bottomTex) {
			bottomAvailableTextures.Add (file);
		}
		foreach (Material mat in bottomMats) {
			if (mat.name != "Middles (Instance)") {
				mat.mainTexture = bottomAvailableTextures [Random.Range (0, midAvailableTextures.Count - 1)];
				bottomAvailableTextures.Remove (mat.mainTexture);
				bottomInUseTextures.Add (mat.mainTexture);
			}
		}
		topTextureName = topMats [0].mainTexture.name;
		midTextureName = midMats [0].mainTexture.name;
		bottomTextureName = bottomMats [0].mainTexture.name;
		audioManager.StartupPlay ();
		UpdateTextTop ();
		UpdateTextMid ();
		UpdateTextBottom ();
	}
		
	public void UpdateTexture(string tagName){
		//print (topSpinner.transform.rotation.eulerAngles.y);
		if (tagName == "topSpinner") {
			if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 180)) {
				UpdateTextureTop (0);
			} else if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 240)) {
				UpdateTextureTop (1);
			} else if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 300)) {
				UpdateTextureTop (2);
			} else if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 0)) {
				UpdateTextureTop (3);
			} else if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 60)) {
				UpdateTextureTop (4);
			} else if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 120)) {
				UpdateTextureTop (5);
			}
			audioManager.UpdateAudioTop ();
		} else if (tagName == "midSpinner") {
			if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 180)) {
				UpdateTextureMid (0);
			} else if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 240)) {
				UpdateTextureMid (1);
			} else if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 300)) {
				UpdateTextureMid (2);
			} else if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 0)) {
				UpdateTextureMid (3);
			} else if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 60)) {
				UpdateTextureMid (4);
			} else if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 120)) {
				UpdateTextureMid (5);
			}
			audioManager.UpdateAudioMid ();
		}else if (tagName == "bottomSpinner") {
			if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 180)) {
				UpdateTextureBottom (0);
			} else if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 240)) {
				UpdateTextureBottom (1);
			} else if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 300)) {
				UpdateTextureBottom (2);
			} else if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 0)) {
				UpdateTextureBottom (3);
			} else if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 60)) {
				UpdateTextureBottom (4);
			} else if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 120)) {
				UpdateTextureBottom (5);
			}
			audioManager.UpdateAudioBottom ();
		}
	}
		
	public void UpdateTextTop(){
		LineOne.text = GameObject.Find(topTextureName.Replace("IMAGE", "TEXT")).GetComponent<Text>().text;
	}

	public void UpdateTextMid(){
		LineTwo.text = GameObject.Find(midTextureName.Replace("IMAGE", "TEXT")).GetComponent<Text>().text;
	}

	public void UpdateTextBottom(){
		LineThree.text = GameObject.Find(bottomTextureName.Replace("IMAGE", "TEXT")).GetComponent<Text>().text;
	}


	//Backside texture update
	public void UpdateTextureTop (int matId) {
		Texture oldTexture = topMats [matId].mainTexture;
		topInUseTextures.Remove (topMats [matId].mainTexture);
		topMats[matId].mainTexture = topAvailableTextures [Random.Range (0, topAvailableTextures.Count - 1)];
		topAvailableTextures.Remove (topMats[matId].mainTexture);
		topInUseTextures.Add (topMats[matId].mainTexture);
		topAvailableTextures.Add (oldTexture);
	}
	public void UpdateTextureMid (int matId) {
		Texture oldTexture = midMats [matId].mainTexture;
		midInUseTextures.Remove (midMats [matId].mainTexture);
		midMats[matId].mainTexture = midAvailableTextures [Random.Range (0, midAvailableTextures.Count - 1)];
		midAvailableTextures.Remove (midMats[matId].mainTexture);
		midInUseTextures.Add (midMats[matId].mainTexture);
		midAvailableTextures.Add (oldTexture);
	}
	public void UpdateTextureBottom (int matId) {
		Texture oldTexture = bottomMats [matId].mainTexture;
		bottomInUseTextures.Remove (bottomMats [matId].mainTexture);
		bottomMats[matId].mainTexture = bottomAvailableTextures [Random.Range (0, bottomAvailableTextures.Count - 1)];
		bottomAvailableTextures.Remove (bottomMats[matId].mainTexture);
		bottomInUseTextures.Add (bottomMats[matId].mainTexture);
		bottomAvailableTextures.Add (oldTexture);
	}
		

	//Spun function to swap all textures
	public void Spun(string transformName){
		if (transformName == "topSpinner") {
			topTempTextures.Clear ();
			foreach (Material mat in topMats) {
				if (mat.name != "Middles (Instance)") {
					topTempTextures.Add (mat.mainTexture);
					topInUseTextures.Remove (mat.mainTexture);
					mat.mainTexture = topAvailableTextures [Random.Range (0, topAvailableTextures.Count - 1)];
					topAvailableTextures.Remove (mat.mainTexture);
					topInUseTextures.Add (mat.mainTexture);
				}
			}
			foreach (Texture textureFile in topTempTextures) {
				topAvailableTextures.Add(textureFile);
			}

		}if (transformName == "midSpinner") {
			midTempTextures.Clear ();
			foreach (Material mat in midMats) {
				if (mat.name != "Middles (Instance)") {
					midTempTextures.Add (mat.mainTexture);
					midInUseTextures.Remove (mat.mainTexture);
					mat.mainTexture = midAvailableTextures [Random.Range (0, midAvailableTextures.Count - 1)];
					midAvailableTextures.Remove (mat.mainTexture);
					midInUseTextures.Add (mat.mainTexture);
				}
			}
			foreach (Texture textureFile in midTempTextures) {
				midAvailableTextures.Add(textureFile);
			}

		}if (transformName == "bottomSpinner") {
			bottomTempTextures.Clear ();
			foreach (Material mat in bottomMats) {
				if (mat.name != "Middles (Instance)") {
					bottomTempTextures.Add (mat.mainTexture);
					bottomInUseTextures.Remove (mat.mainTexture);
					mat.mainTexture = bottomAvailableTextures [Random.Range (0, bottomAvailableTextures.Count - 1)];
					bottomAvailableTextures.Remove (mat.mainTexture);
					bottomInUseTextures.Add (mat.mainTexture);
				}
			}
			foreach (Texture textureFile in bottomTempTextures) {
				bottomAvailableTextures.Add(textureFile);
			}
			}
	}
		
}
