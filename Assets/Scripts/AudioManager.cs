﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public float timerSync = 2;
	private float timer = 0;
	private float topTimer = 0;
	private float midTimer = 0;
	private float bottomTimer = 0;
	private GameObject topSpinner;
	private GameObject midSpinner;
	private GameObject bottomSpinner;
	private AudioSource topSource;
	private AudioSource midSource;
	private AudioSource bottomSource;
	private AudioClip currentTop;
	private AudioClip currentMid;
	private AudioClip currentBottom;
	private AudioClip newTop;
	private AudioClip newMid;
	private AudioClip newBottom;
	public bool newTopQueued = false;
	public bool newMidQueued = false;
	public bool newbottomQueued = false;
	private TextureSwap textureSwap;
	public string topTextureName;
	public string midTextureName;
	public string bottomTextureName;

	void Awake(){
		topSpinner = GameObject.FindGameObjectWithTag ("topSpinner");
		midSpinner = GameObject.FindGameObjectWithTag ("midSpinner");
		bottomSpinner = GameObject.FindGameObjectWithTag ("bottomSpinner");
		textureSwap = GameObject.Find ("TextureManager").GetComponent<TextureSwap> ();
	}

	// Use this for initialization
	public void StartupPlay () {
		topSource = topSpinner.GetComponent<AudioSource> ();
		currentTop = Resources.Load<AudioClip> ("TopAudio/" + textureSwap.topTextureName.Replace("IMAGE", "AUDIO"));
		midSource = midSpinner.GetComponent<AudioSource> ();
		currentMid = Resources.Load<AudioClip> ("MIDAudio/" + textureSwap.midTextureName.Replace("IMAGE", "AUDIO"));
		bottomSource = bottomSpinner.GetComponent<AudioSource> ();
		currentBottom = Resources.Load<AudioClip> ("BottomAudio/" + textureSwap.bottomTextureName.Replace("IMAGE", "AUDIO"));
		topSource.clip = currentTop;
		midSource.clip = currentMid;
		bottomSource.clip = currentBottom;
		topSource.Play ();
		midSource.Play ();
		bottomSource.Play();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		topTimer += Time.deltaTime;
		midTimer += Time.deltaTime;
		bottomTimer += Time.deltaTime;
		if (timer >= timerSync) {
			timer = 0;
			//print ("sync point");
		}
		if (topTimer >= 4) {
			topTimer = 0;
			topSource.Play ();
			//print ("reset top");
		}
		if (midTimer >= 4) {
			midTimer = 0;
			midSource.Play ();
			//print ("reset mid");
		}
		if (bottomTimer >= 4) {
			bottomTimer = 0;
			bottomSource.Play ();
			//print ("reset bottom");
		}

		if (newTopQueued) {
			textureSwap.UpdateTextTop ();
			topSource.loop = false;
			if (timer <= 0 || timer >= timerSync) {
				newTopQueued = false;
				topSource.clip = newTop;
				currentTop = newTop;
				topSource.Play ();
				topSource.loop = true;
			} 
		}

		if (newMidQueued) {
			textureSwap.UpdateTextMid ();
			midSource.loop = false;
			if (timer <= 0 || timer >= timerSync) {
				newMidQueued = false;
				midSource.clip = newMid;
				currentMid = newMid;
				midSource.Play ();
				midSource.loop = true;
			}
		}

		if (newbottomQueued) {
			textureSwap.UpdateTextBottom ();
			bottomSource.loop = false;
			if (timer <= 0 || timer >= timerSync)  {
				newbottomQueued = false;
				bottomSource.clip = newBottom;
				currentBottom = newBottom;
				bottomSource.Play ();
				bottomSource.loop = true;
			}
		}
	}

	// Audio updates
	public void UpdateAudioTop(){
		if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 180)) {
			topTextureName = textureSwap.topMats [3].mainTexture.name;
		} else if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 240)) {
			topTextureName = textureSwap.topMats [4].mainTexture.name;
		} else if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 300)) {
			topTextureName = textureSwap.topMats [5].mainTexture.name;
		} else if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 0)) {
			topTextureName = textureSwap.topMats [0].mainTexture.name;
		} else if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 60)) {
			topTextureName = textureSwap.topMats [1].mainTexture.name;
		} else if (Mathf.Approximately (topSpinner.transform.rotation.eulerAngles.y, 120)) {
			topTextureName = textureSwap.topMats [2].mainTexture.name;
		}
		string newTopClip = topTextureName.Replace("IMAGE", "AUDIO");
		newTop = Resources.Load<AudioClip> ("TopAudio/" + newTopClip);
		newTopQueued = true;
	}
	public void UpdateAudioMid(){
		if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 180)) {
			midTextureName = textureSwap.midMats [3].mainTexture.name;
		} else if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 240)) {
			midTextureName = textureSwap.midMats [4].mainTexture.name;
		} else if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 300)) {
			midTextureName = textureSwap.midMats [5].mainTexture.name;
		} else if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 0)) {
			midTextureName = textureSwap.midMats [0].mainTexture.name;
		} else if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 60)) {
			midTextureName = textureSwap.midMats [1].mainTexture.name;
		} else if (Mathf.Approximately (midSpinner.transform.rotation.eulerAngles.y, 120)) {
			midTextureName = textureSwap.midMats [2].mainTexture.name;
		}
		string newMidClip = midTextureName.Replace("IMAGE", "AUDIO");
		newMid = Resources.Load<AudioClip> ("MidAudio/" + newMidClip);
		newMidQueued = true;
	}
	public void UpdateAudioBottom(){
		if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 180)) {
			bottomTextureName = textureSwap.bottomMats [3].mainTexture.name;
		} else if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 240)) {
			bottomTextureName = textureSwap.bottomMats [4].mainTexture.name;
		} else if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 300)) {
			bottomTextureName = textureSwap.bottomMats [5].mainTexture.name;
		} else if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 0)) {
			bottomTextureName = textureSwap.bottomMats [0].mainTexture.name;
		} else if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 60)) {
			bottomTextureName = textureSwap.bottomMats [1].mainTexture.name;
		} else if (Mathf.Approximately (bottomSpinner.transform.rotation.eulerAngles.y, 120)) {
			bottomTextureName = textureSwap.bottomMats [2].mainTexture.name;
		}
		string newBottomClip = bottomTextureName.Replace("IMAGE", "AUDIO");
		newBottom = Resources.Load<AudioClip> ("BottomAudio/" + newBottomClip);
		newbottomQueued = true;
	}
}
